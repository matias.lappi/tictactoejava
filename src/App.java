import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner consoleInput = new Scanner(System.in);
        String[][] board = new String[3][3];
        printGameBoard(board);
        String currentPlayer = "X";

        while (true) {
            System.out.print("Your move: ");
            if (consoleInput.hasNext()) {
                String move = consoleInput.next();

                if (move.length() == 2) {
                    int row = Character.getNumericValue(move.charAt(0) - 1);
                    int col = Character.getNumericValue(move.charAt(1) - 1);

                    if (row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == null) {
                        board[row][col] = currentPlayer;
                        printGameBoard(board);

                        if (checkForWinner(board)) {
                            System.out.println("Player " + currentPlayer + " wins!");
                            break;
                        } else if (isBoardFull(board)) {
                            System.out.println("It's a draw!");
                            break;
                        }

                        currentPlayer = (currentPlayer.equals("X")) ? "O" : "X";
                    } else {
                        System.out.println("Invalid move. Try again.");
                    }
                } else {
                    System.out.println("Invalid move. Try again.");
                }
            } else {
                break;
            }
        }
        consoleInput.close();
    }

    private static void printGameBoard(String[][] board) {
        for (int i = 0; i < 3; i++) {
            System.out.println("-------------");
            for (int j = 0; j < 3; j++) {
                System.out.print("|");
                if (board[i][j] == null) {
                    System.out.print("   ");
                } else {
                    System.out.print(" " + board[i][j] + " ");
                }
            }
            System.out.println("|");
        }
        System.out.println("-------------");
    }

    private static boolean checkForWinner(String[][] board) {
        // Checks rows and columns
        for (int i = 0; i < 3; i++) {
            if ((board[i][0] != null && board[i][0].equals(board[i][1]) && board[i][1].equals(board[i][2])) ||
                (board[0][i] != null && board[0][i].equals(board[1][i]) && board[1][i].equals(board[2][i]))) {
                return true;
            }
        }
        // Checks diagonals
        if ((board[0][0] != null && board[0][0].equals(board[1][1]) && board[1][1].equals(board[2][2])) ||
            (board[0][2] != null && board[0][2].equals(board[1][1]) && board[1][1].equals(board[2][0]))) {
            return true;
        }
        return false;
    }

    private static boolean isBoardFull(String[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == null) {
                    return false;
                }
            }
        }
        return true;
    }
}